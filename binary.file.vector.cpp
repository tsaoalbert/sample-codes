#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <unordered_set>

using namespace std;


class Rectangle {
  int m_width = 0 ;
  int m_height = 0 ; 
  int m_x = 0 ;
  int m_y = 0 ;
public:
  friend ostream& operator<< (ostream& out, const Rectangle& r ) ;
  Rectangle (): Rectangle(0,0,0,0) {
  };
  Rectangle (int w,int h,int x,int y){
     m_width = w ;
     m_height = h ; 
     m_x = x ;
     m_y = y ;
  };
};

ostream& operator<< (ostream& out, const Rectangle& r ) {
  out << "(" << r.m_width << ", ";
  out << "" << r.m_height << ", ";
  out << "" << r.m_x << ", ";
  out << "" << r.m_y << ") ";
  return out;
}

template
< typename T
    , template<typename ELEM, typename ALLOC=std::allocator<ELEM> > class Container
>
std::ostream& operator<< (std::ostream& o, const Container<T>& container)
{
    for_each( cbegin(container), cend(container), [&o](auto c) {o << c << " ";} );
    return o;
}

template
< typename T, 
    template<typename T2, class Compare = less<T2>, typename ALLOC=std::allocator<T2> > class Container
>
std::ostream& operator<< (std::ostream& o, const Container<T>& container)
{
    for_each( cbegin(container), cend(container), [&o](auto c) {o << c << " ";} );

    return o;
}


template<typename T>
ostream& operator<< (ostream& out, const unordered_set <T>& s ) {
  for_each( cbegin(s), cend(s), [&out](auto c) {out << c << " ";} );
  return out;
}



template<typename T, template <typename... Args> class Container >
int writebinaryFile ( const Container<T> & v, const string& fn ) {

  ofstream file ;
  file.open( fn,ios::binary);
  
  size_t n = v.size();
  file.write((char*)&n, sizeof(n)  );

  const auto & x = *v.begin(); // first elelment
  char* ptr = (char*) & ( x );
  file.write((char*)&x, sizeof( T )*n );

  file.close();
  cout<< n << " data saved into file " << fn << ": " << v << endl;
  return 1;
}
  
template<typename T, template <typename... Args> class Container >
int readbinaryFile ( const Container<T> & v, const string& fn) {
  ifstream file ; //open file again 
  file.open(fn,ios::binary);

  size_t n ;
  file.read( (char*)&n, sizeof( n ) ) ;

  if ( file.good() ) {
    const auto & x = *( v.begin());
    file.read((char*)&x,sizeof( T )* n )  ;
  }
  
  if( file.bad() ) {
    cout<<"Error in reading data from file...\n";
    return -1;
  }
  file.close(); 
  cout<< n << " data read from file " << fn << ": " << v << endl;
  return 0;
}
int main ()  {
  string fn = "emp.dat" ;
  size_t n = 10;
  vector<int > v (n);
  std::generate(v.begin(), v.end(), []() { return rand()%100; });

  writebinaryFile ( v, fn  );
  readbinaryFile (v, fn   );

  list<int> tmp (v.begin(), v.end());
  writebinaryFile ( tmp, fn  );
  readbinaryFile (tmp, fn   );

  set<int> s (v.begin(), v.end());
  writebinaryFile ( s, fn  );
  readbinaryFile (s, fn   );


  unordered_set<int> u (v.begin(), v.end());
  writebinaryFile ( u, fn  );
  readbinaryFile (u, fn   );

  vector<Rectangle> w (10 );
  generate(w.begin(), w.end(), []() { return Rectangle(rand()%10,rand()%10,rand()%10,rand()%10); } ) ;
  cout << w << endl;
  writebinaryFile ( w, fn  );
  readbinaryFile (w, fn   );
}
