#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <unordered_set>

using namespace std;


class Rectangle {
  int m_width = 0 ;
  int m_height = 0 ; 
  int m_x = 0 ;
  int m_y = 0 ;
public:
  friend ostream& operator<< (ostream& out, const Rectangle& r ) ;
  Rectangle (): Rectangle(0,0,0,0) {
  };
  Rectangle (int w,int h,int x,int y){
     m_width = w ;
     m_height = h ; 
     m_x = x ;
     m_y = y ;
  };
};

int writebinaryFile ( const vector<Rectangle>& v, const string& fn ) {

  
  ofstream file ;
  file.open( fn,ios::binary);
  
  size_t n = v.size();
  file.write((char*)&n, sizeof(n)  );

  const auto & x = *v.begin(); // first elelment
  char* ptr = (char*) & ( x );
  file.write((char*)&x, sizeof( Rectangle )*n );

  file.close();
  cout<< n << " data saved into file " << fn << ": " << endl;
  return 1;
}
  
int readbinaryFile ( const vector<Rectangle> & v, const string& fn) {
  ifstream file ; //open file again 
  file.open(fn,ios::binary);

  size_t n ;
  file.read( (char*)&n, sizeof( n ) ) ;

  if ( file.good() ) {
    const auto & x = *( v.begin());
    file.read((char*)&x,sizeof( Rectangle )* n )  ;
  }
  
  if( file.bad() ) {
    cout<<"Error in reading data from file...\n";
    return -1;
  }
  file.close(); 
  cout<< n << " data read from file " << fn << ": " << endl;
  return 0;
}
int main ()  {
  const string fn ( "emp.dat" );
  vector<Rectangle> w (10 );
  generate(w.begin(), w.end(), []() { return Rectangle(rand()%10,rand()%10,rand()%10,rand()%10); } ) ;
  writebinaryFile ( w, fn  );
  readbinaryFile (w, fn   );
}
