#include <algorithm>
#include <random>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <iterator>
#include <fstream>
#include <sstream>
#include <iostream>


using namespace std;

// Overload insertion operator << to output elements of the map, i.e., pair<S,T>
// TODO: fill in the missing piece of code

// Overload insertion operator << for string
// TODO: fill in the missing piece of code

// Overload insertion operator << for C-Style string (i.e., char[])
  // TODO: fill in the missing piece of code

// Overload insertion operator << to output the content of any STL container
// TODO: fill in the missing piece of code

int main () 
{

 cout << "Uncomment the following statements to test your implementation" << endl;
/*
  vector<int> vec = { 1,2,3};
  list<int> L = { 1,2,3};
  unordered_set<int> unordered = { 1,2,3};
  set<int> s = { 1,2,3};
  cout << "vec = " << vec << endl; 
  cout << "list = " << L << endl; 
  cout << "unordered = " << unordered << endl; 
  cout << "set = " << s << endl; 
  map<int,string> m = { {1,"one"}, {2,"two"}, {3,"three"}  };
  cout << "map = " << m << endl;
  unordered_map<int,string> um = { {1,"one"}, {2,"two"}, {3,"three"}  };
  cout << "unordered map = " << um << endl;
*/
}

