
#include <iostream>
#include <map>
#include <unordered_map>
#include <cmath>
#include <set>
#include <unordered_set>
#include <vector>
#include <algorithm>
using namespace std;

template <typename T>
void printVector1 ( const vector< T > & v ) {
    for ( auto x: v ) {
        cout << x << ", " ;
    }
    cout << endl ;
    // cout << "=================" << endl ;
}

void trace ( int i, const string & final, const vector<string> &move, const vector<int> &parent, const vector<int> & jump ) 
{
  int cnt = 1 ;
  int length = 0 ;
  string msg = to_string (cnt++) + ":" + final  + " ---> " ;
  // string msg = to_string (cnt++) + ":" + " ---> " ;
  while ( i >= 0 ) {
    msg += to_string (cnt) + ":" + move[i] ;
    msg += " (" + to_string ( jump[i] ) + ")";
    if ( i> 0 ) msg += " ---> " ;
    i = parent [i];
    cnt ++;
    if ( msg.size() >= 80  || i ==-1) {
      cout << msg << endl ;
      msg = "";
    }
  }
}

int playGame ( ) {
  int stop  = INT_MAX;
  vector <string> move ;
  set < string  > mySet ;
  vector <int > parent ; // previous state
  vector <int > jump ; // previous state
  vector <int > empty ;
  vector <int > level ;

  string first = "AA-ZZ";
  string final = "ZZ-AA";

  move.push_back (first );
  parent.push_back ( -1 );
  empty.push_back ( 2 );
  mySet.insert ( first );
  level.push_back (0);
  jump.push_back (0);

  int i = 0 ;
  while ( i < move.size() && level[i] <= stop )   { 
    const int delta [] = {-2,-1,1,2} ;
    for ( int j = 0 ; j < 4; ++j ) {
      // if ( i==0 && j<2 ) continue;
      const int k = delta [j]; 
      int m = empty[ i] ;
      if ( m+k <0 || m+k >= move[i].size() ) continue ;
      // leaf from m+k to m
      string next = move[i];
      // cout << i << ": " << move[i] << ", k=" << k << endl;
      // cout << m+k << ": " << move[m+i] << ", k=" << k << endl;
      if ( next[m+k] == 'Z' && k < 0 ) continue ; // A cannot move backward
      if ( next[m+k] == 'A' && k > 0 ) continue ; // Z cannot move froward
      next[m+ k] = '-'; // swap next[m+k] and next[m]
      next[m] = move[i][m + k];
      if ( mySet.find ( next ) == mySet.end() ) {
        mySet.insert (    next  );
        move.push_back ( next );
        empty.push_back ( m+k );
        parent.push_back ( i );
        jump.push_back ( m + k ) ;
        level.push_back ( level[i]+1 );
      }
      if ( next==final ) {
        trace ( i, final, move, parent, jump );
        stop = min ( level[i], stop ) ; 
      } 
    }
    i++ ;
  } 
  int pre = -1;
  for (int i=0; i < move.size(); ++i ) {
    if ( level[i]!= pre ) {
      pre = level [i]; 
      cout << endl << pre << ": " ;
    }
    cout << move [i] << " " ;
  }
  cout << endl;
  return move.size ();
}


int main ( ) {
    playGame ( );
}


/*
Task 1 (3 points):

Write the following toads and frogs game.
Toads and frogs is a one player game. Given a board of size N (where N is odd), left side of the board is filled with toads and right side is with frogs and the middle square is empty. Here is the initial board configuration for N=7:

T T T - F F F

'T': toad, 'F': frog, '-': empty

A toad can slide right (to the empty spot) or 
jump over one square to the empty spot on its right. 

Similarly, a frog can slide left or jump over one square to the left. 
The objective of the game is to move the toads to the right side and frogs to the left side of the board in minimum number of moves:

last                curr = last; when undo

F F F - T T T  ==> F F - F T T T (  slide to left )
               ==> F F F T - T T ( slide to right )
               ==> F - F F T T T (  jump over to left )
               ==> F F F T T - T (  jump over to right )
*/

/* 
Game design:
The player will enter the board size N. Then you will setup the board with respect to N.
In each turn, print the current board and number of moves on screen.

Then the program waits for the player input, a number between 1 to N. The number indicates to move the element in that square. For instance, 3 moves the frog at position 3 to right.
Continue asking the player move if the provided move is not legal. Otherwise make the move.
The game finishes whenever all frogs are on the left side and 
all toads are on the right side and the empty square is in the middle.

Task 2 (2 points):
Add the undo move option; '0' means undo.  
The player should be able to undo till the beginning of the game.  
Extension: toadfrog-computer.cpp (3 points) 
Write the game that is played by computer.  
*/
/*
F F F - T T T  ==> F F - F T T T (  slide to left )
               ==> F F F T - T T ( slide to right )
               ==> F - F F T T T (  jump over to left )
               ==> F F F T T - T (  jump over to right )
*/

