#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <set>
#include <unordered_set>
using namespace std;

class Animal {
public:
  virtual void feed () const  = 0 ;
};

class Lion: public Animal {
public:
  virtual void feed () const  {
     cout << "Lion eats meat" << endl;
  }
};
class Horse: public Animal {
public:
  virtual void feed () const  {
     cout << "Horse eats veggie!" << endl;
  }
};
int main () {
  Lion simba;
  Horse sprinter;
  vector<Animal*> zoo = { &simba, &sprinter } ;
  for ( const auto& x: zoo ) {
     x->feed ();
  }
}
/*
  Base& b1 = d; // copy derived to Base
  // cout << b1.m_id << endl; // not legal
  Derived& d1 = (Derived&) b1;
  cout << d1.m_id << endl; // legal or not
*/
