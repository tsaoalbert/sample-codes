#include <algorithm>
#include <random>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <iterator>
#include <fstream>
#include <sstream>
#include <iostream>


using namespace std;

// Overload insertion operator << to output elements of the map, i.e., pair<S,T>
template<
  typename S,
  typename T
>
ostream& operator<< (ostream& out, const pair<S,T>& p ) {
  out << p.first << ":" << p.second  ;
  return out;
}

// Overload insertion operator << for string
ostream& operator<< (ostream& out, const string& s ) {
  for (auto& c:s) out << c ;
  return out;
}
// Overload insertion operator << with for C-Style string (i.e., char[])
ostream& operator<< (ostream& out, const char s[] ) {
  string ss(s);
  out << ss;
  return out;
}
// Overload insertion operator << to output the content of any STL container
template < class Container >
ostream& operator<< (ostream& out, const Container& s ) {
    out << "[" ;
    unsigned i = 0 ;
    for (auto& c:s) {
      if ( i>0) out << ", " ;
      out << c ; 
      ++i;
    }
    /* for_each( cbegin(s), cend(s), [&out, &i](const auto& c) {
      if ( i>0) out << ", " ; out << c ; ++i; }  ); */
    out << "]";
  return out;
};

template <typename C>
std::string format( const C & s) {
    ostringstream out; 
    out << s ;

    return out.str();
}


static const size_t NOT_FOUND = (size_t)(-1); 


template <typename T>
size_t Find(  T& x,      T* arr, size_t n)
{
  for (size_t i = 0; i < n; i++) { 
    if (arr[i] == x) {
      return i; // Found it; return the index 
    }
  }
  return NOT_FOUND; 
}

void unitTestTemplateFunction () {
  int x = 3, arr[]= {1, 2, 3, 4};
  size_t res, n,i,j;
  n= sizeof( arr ) / sizeof(int);

  i= Find(x, arr, n);                  // calls Find<int> by deduction
  j= Find<int>(x,  arr , n);   // calls Find<int> explicitly

  double y= 5.6, dArr[] = {1.2, 3.4, 5.7, 7.5};

  n= sizeof(dArr) / sizeof(double);
  res = Find(y, dArr, n);                   // calls Find<double> by deduction
  res = Find<double> (y, dArr, n);   // calls Find<double> explicitly

  // res = Find(3, dArr, n);  // DOES NOT COMPILE!  Arguments are different types.
}
class MyInteger {
public:
  MyInteger (int i): x(i) {} ;
  int x;
};
ostream& operator<< (ostream& out, const MyInteger & s) {
  out << s.x ;
  return out;
}

void unitTestOutput () 
{
  vector<int> vec = { 1,2,3};
  list<int> L = { 1,2,3};
  unordered_set<int> unordered = { 1,2,3};
  set<int> s = { 1,2,3};
  cout << "vec = " << vec << endl; 
  cout << "list = " << L << endl; 
  cout << "unordered = " << unordered << endl; 
  cout << "set = " << s << endl; 
  map<int,string> m = { {1,"one"}, {2,"two"}, {3,"three"}  };
  cout << "map = " << m << endl;
  unordered_map<int,string> um = { {1,"one"}, {2,"two"}, {3,"three"}  };
  cout << "unordered map = " << um << endl;
  MyInteger i (0);
  cout << "MyInteger = " << i << endl;
  cout << "unordered map = " << um << endl;
/*
  cout << "format(s) = " << format(s) << endl; 
*/
}

int main () {
 unitTestOutput ()  ;
 unitTestTemplateFunction () ;
}
