#include <algorithm>
#include <random>
#include <vector>
#include <list>
#include <set>
#include <unordered_set>
#include <map>
#include <iterator>
#include <fstream>
#include <sstream>
#include <iostream>


using namespace std;

/*

The purpose of this lab is to exercise the following concepts:
* template varidiac parameter
* template template parameter

*/


// template speciliation for string
ostream& operator<< (ostream& out, const string& s ) {
  for (auto& c:s) out << c ;
  return out;
}

template<
  typename S,
  typename T 
>
ostream& operator<< (ostream& out, const pair<S,T>& p ) {
  out << p.first << ":" << p.second  ;
  return out;
}

/*
*/
template<
  typename S,
  typename T
>
ostream& operator<< (ostream& out, const map<S,T>& s ) {
  out << "[";
  bool first = true;
  for_each( cbegin(s), cend(s), [&out,&first](auto c) {if (!first) out << ", " ; out << c ; first = false ;} );
  out << "]";
 
  return out;
}

template<
  typename S,
  template <typename... Args> class Container=map
>
ostream& operator<< (ostream& out, const Container<S>& s ) {
  out << "[";
  bool first = true;
  for_each( cbegin(s), cend(s), [&out,&first](auto c) {if (!first) out << ", " ; out << c ; first = false ;} );
  out << "]";
  
  return out;
}

template <typename C>
std::string format( const C & s) {
    ostringstream out;
    out << s ;

    return out.str();
}


void unitTestOutput () 
{
  vector<double> vec = { 1.1,2.2,3.3};
  list<int> lst = { 1,2,3};
  unordered_set<int> unorderedSet = { 1,2,3};
  set<int> orderedSet = { 1,2,3};
  map<int, string> m = { {0, "zero"}, {1, "one"} };
  // map<int, int> m = { {0, 0     }, {1, 1    } };

  cout << "vec = " << vec << endl; 
  cout << "lst = " << lst << endl; 
  cout << "unordered set = " << unorderedSet << endl; 
  cout << "ordered set = " << orderedSet << endl; 
  cout << "format( unorderedSet ) = " << format( unorderedSet ) << endl; 
/*
*/
  cout << m << endl; 
  cout << "map = " << m << endl; 
}

int main () {
 unitTestOutput ()  ;
}
