#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <array>
#include <set>
#include <unordered_set>
using namespace std;

class Base {
public:
  virtual void foo () {
    cout << "Base: " << __func__ << endl; 
    bar(); 
  }
  // with    virtual, bar is to be overridde in Derived
  // without virtual, bar is to be redefined in Derived
  virtual void bar () const {
    cout << "Base: " << __func__ << endl; // __func__ = current function name
  }
};

class Derived: public Base {
public:
  int m_id = 0 ;
  void bar () const override { // 
    cout << "Derived: " << __func__ << endl; // __func__ = current function name
  }
};
void testPolymorphism () {
  Base b;
  Derived d;
  vector <Base*> v = { &b, &d } ;
  for (auto& w:v ) {
      w->bar();
  }
}
  
void testOverride () {
  Base b;
  Derived d;
  Base& b1 = d; // b1 is a reference
  Base b2 = d; // copy derived to Base
  // cout << b1.m_id << endl; // not legal
  // cout << b2.m_id << endl; // not legal
  Derived& d1 = dynamic_cast<Derived&> (b1);
  cout << d1.m_id << endl; // legal 
}

int main () {
  // testPolymorphism () ;
  // testOverride () ;
  // array< int, 3> v = { 1,2,3};
  // vector< int> v = { 1, 2, 3, 4};
  vector< int> v (10,9); 
  // vector< int> u  = v;
  // vector< int> u  (v);
  vector< int> u  ( begin(v), end(v) - v.size()/2 );

  sort ( begin(v), end(v) );
  // reverse ( begin(v), end(v) );

  // cout << *max_element ( begin(v), end(v) ) << endl;
  // cout << *min_element ( begin(v), end(v) ) << endl;

  for (int i=0; i <= v.size() ; ++i ) {
    // cout << v[i] << endl;
  }
  for ( auto& x: v ) {
    // cout << x << endl;
  }
  for ( auto it = rbegin(v); it != rend(v) ; ++it ) {
    // cout << *it << endl;
  }
  copy (crbegin(v), crend(v)-1, ostream_iterator<int>(cout, ", " ));
  cout << *(--crend(v)) << endl;

  copy (crbegin(u), crend(u)-1, ostream_iterator<int>(cout, ", " ));
  cout << *(--crend(u)) << endl;


}
