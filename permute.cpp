#include <numeric>
#include <functional>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

template<typename T,
    template <typename, typename = allocator<T>> class Container>
ostream &operator<<(ostream &out, const Container<T> &v) {
  for (auto &el : v)
  out << el << ' ';
  return out;
} // operator<<()

/*
template<typename T>
ostream &operator<<(ostream &out, const vector<T> &v) {
  for (auto &el : v)
  out << el << ' ';
  return out;
} // operator<<()
*/

template<typename T>
void genPerms(deque<T> &unused, vector<T> &perm) {
  cout << "unused (queue) = " << unused ;
  cout << ",  perm (stack) = " << perm << '\n';
  // perm is only a "prefix" until unused is empty
  if (unused.empty()) {
    // cout << perm << '\n';
    cout << '\n';
    return;
  } // if
  for (unsigned k = 0; k != unused.size(); k++) {
    perm.push_back(unused.front());
    unused.pop_front();
    genPerms(unused, perm);
    unused.push_back(perm.back());
    perm.pop_back();
  } // for
} // genPerms()

int main() {

  unsigned n;
  string junk;
  cout << "Enter n: " << flush;
  while (!(cin >> n)) {
    cin.clear();
    getline(cin, junk);
    cout << "Enter n: " << flush;
  } // while
  vector<unsigned> perm;
  deque<unsigned> unused(n);
  iota(unused.begin(), unused.end(), 1);
  genPerms(unused, perm);
  return 0;
} // main()
